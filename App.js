// react
import React from "react";
import { useFonts } from "@expo-google-fonts/inter";

// redux
import { Provider } from "react-redux";
import { store, persistor } from "./src/redux/store";
import { PersistGate } from "redux-persist/integration/react";

// navigation
import NavContainer from "./src/navigation/NavContainer";

export default function App() {
  const [fontsLoaded] = useFonts({
    "Lato-400": require("./src/assets/fonts/Lato/Lato-Regular.ttf"),
    "Lato-700": require("./src/assets/fonts/Lato/Lato-Bold.ttf"),
    "Lato-900": require("./src/assets/fonts/Lato/Lato-Black.ttf"),
    "Poppins-500": require("./src/assets/fonts/Poppins/Poppins-Medium.ttf"),
    "Poppins-700": require("./src/assets/fonts/Poppins/Poppins-Bold.ttf"),
    "Poppins-800": require("./src/assets/fonts/Poppins/Poppins-ExtraBold.ttf"),
  });

  return (
    fontsLoaded && (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavContainer></NavContainer>
        </PersistGate>
      </Provider>
    )
  );
}
