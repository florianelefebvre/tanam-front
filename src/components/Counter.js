// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";

const Counter = ({ theme, quantity, incFunc, decFunc }) => {
  return (
    <StyledCounter>
      <Button
        theme={theme}
        onPress={decFunc}
        disabled={quantity < 1 ? true : false}
      >
        <Label theme={theme}>-</Label>
      </Button>
      <Count theme={theme}>{quantity}</Count>
      <Button
        theme={theme}
        onPress={incFunc}
        disabled={quantity >= 10 ? true : false}
      >
        <Label theme={theme}>+</Label>
      </Button>
    </StyledCounter>
  );
};

export default Counter;

const StyledCounter = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

const Button = styled.TouchableOpacity`
  background-color: ${(props) =>
    props.theme === "light" ? "transparent" : " #027335"};
  border: ${(props) =>
    props.theme === "light" ? "solid 1px #E8EFF3" : "none"};
  height: ${(props) => (props.theme === "light" ? "42px" : "36px")};
  width: ${(props) => (props.theme === "light" ? "42px" : "36px")};
  border-radius: 12px;
`;

const Label = styled.Text`
  color: ${(props) => (props.theme === "light" ? "#303733" : "white")};
  font-family: Lato-700;
  font-size: ${(props) => (props.theme === "light" ? "30px" : "24px")};
  text-align: center;
`;

const Count = styled.Text`
  color: ${(props) => (props.theme === "light" ? "black" : "#027335")};
  font-family: Lato-400;
  font-size: 18px;
`;
