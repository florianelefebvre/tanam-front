// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";

// styles & assets
import { H1 } from "../styles/global.styles";

//icons
import icons from "../assets/icons";
const { goBackArrowBlackIcon, goBackArrowWhiteIcon } = icons;

const GoBack = ({ title, icon, onPressArrowFunc, onPressIconFunc, color }) => {
  return (
    <StyledGoBack>
      <ArrowTitleView>
        <ArrowBack onPress={onPressArrowFunc}>
          <Text>{color ? goBackArrowWhiteIcon : goBackArrowBlackIcon}</Text>
        </ArrowBack>
        <H1>{title}</H1>
      </ArrowTitleView>
      <IconButton onPress={onPressIconFunc}>{icon}</IconButton>
    </StyledGoBack>
  );
};
export default GoBack;

const StyledGoBack = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const ArrowTitleView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const ArrowBack = styled.TouchableOpacity`
  margin-right: 20px;
`;

const IconButton = styled.TouchableOpacity``;
