// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";

const OrderStatusCard = ({ title, date }) => {
  return (
    <StyledOrderStatusCard>
      <OrderStatusDot date={date}></OrderStatusDot>
      <OrderStatusDescription>
        <Title date={date}> {title}</Title>
        <Date>{date}</Date>
      </OrderStatusDescription>
    </StyledOrderStatusCard>
  );
};
export default OrderStatusCard;

const StyledOrderStatusCard = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin: 5px 0;
`;

const Title = styled.Text`
  font-family: Lato-700;
  font-size: 16px;
  color: ${(props) => (props.date ? "#303733" : "#7D8FAB")};
`;

const Date = styled.Text`
  font-family: Lato-400;
  font-size: 12px;
  margin-top: 5px;
  color: #7d8fab;
`;

const OrderStatusDot = styled.View`
  height: 14px;
  width: 14px;
  border-radius: 12px;
  background-color: ${(props) => (props.date ? "#027335" : "#7D8FAB")};
  margin-top: 3px;
`;
const OrderStatusDescription = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: 20px;
`;
