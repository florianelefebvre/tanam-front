// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useState } from "react";
import { WBS_URL } from "../utils/globalVars";

// icons
import icons from "../assets/icons";
const { filledHeartIcon, clearHeartIcon, shoppingCartIcon, discountIcon } =
  icons;

const ProductCard = ({ product, onPressFunc }) => {
  const [liked, setLiked] = useState(false);

  return (
    <StyledProductCard>
      <ImageView
        style={{ flex: 1 }}
        source={{
          uri: product.img_url
            ? `${WBS_URL}${product.img_url}`
            : "https://media.istockphoto.com/photos/fresh-fruit-display-in-open-food-market-in-rome-italy-picture-id492262592",
        }}
      ></ImageView>
      <Heart
        onPress={() => {
          setLiked(!liked);
        }}
      >
        {liked ? (
          <Text> {filledHeartIcon}</Text>
        ) : (
          <Text> {clearHeartIcon}</Text>
        )}
      </Heart>

      <ProductDetail>
        <ProductName>{product.name}</ProductName>

        {product.priceAfterDiscount ? (
          <PriceView>
            <PriceAfterDiscount>
              $ {product.priceAfterDiscount}
            </PriceAfterDiscount>
            <Price textDecoration>$ {product.price}</Price>
          </PriceView>
        ) : (
          <Price>$ {product.price}</Price>
        )}
        {product.discount !== 0 && (
          <DiscountView>
            <Text> {discountIcon} </Text>
            <Discount>Disc. {product.discount * 100}%Off</Discount>
          </DiscountView>
        )}
      </ProductDetail>
      <Button onPress={onPressFunc}>{shoppingCartIcon}</Button>
    </StyledProductCard>
  );
};
export default ProductCard;

const StyledProductCard = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  padding: 20px 0;
`;

const ImageView = styled.Image`
  background-color: #c4c4c4;
  border-radius: 12px;
  height: 100px;
  flex: 1;
`;

const ProductDetail = styled.View`
  flex: 2;
  padding-left: 10px;
`;

const ProductName = styled.Text`
  color: #303733;
  font-size: 18px;
  font-family: Lato-900;
  text-align: left;
  padding: 10px;
`;

const PriceView = styled.View`
  display: flex;
  flex-direction: row;
`;

const Price = styled.Text`
  color: ${(props) => (props.textDecoration ? "#bfc9da" : "#293041")};
  font-family: Poppins-500;
  font-size: 14px;
  margin: 10px;
  text-decoration: ${(props) => (props.textDecoration ? "line-through" : "")};
  text-decoration-color: #bfc9da;
`;

const PriceAfterDiscount = styled.Text`
  color: #293041;
  font-family: Poppins-500;
  font-size: 14px;
  margin: 10px;
`;

const DiscountView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Discount = styled.Text`
  color: #c29c1d;
  font-family: Lato-400;
  font-size: 12px;
  margin-left: 5px;
`;

const Heart = styled.TouchableOpacity`
  position: absolute;
  top: 30px;
  left: 5px;
`;

const Button = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: #027335;
  height: 45px;
  width: 45px;
  border-radius: 12px;
`;
