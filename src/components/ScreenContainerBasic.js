// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";

const ScreenContainer = ({
  children,
  statusBarStyle,
  header,
  backgroundColor,
  tabBar,
  footer,
}) => {
  return (
    <SafeAreaView
      style={{ backgroundColor, flex: 1 }}
      edges={tabBar ? ["right", "top", "left"] : undefined}
    >
      <StyledScreenContainer>
        <StatusBar style={statusBarStyle}></StatusBar>
        <HeaderSection>{header}</HeaderSection>
        <ChildrenSection>{children}</ChildrenSection>
        <FooterSection>{footer}</FooterSection>
      </StyledScreenContainer>
    </SafeAreaView>
  );
};

ScreenContainer.defaultProps = {
  statusBarStyle: "dark",
  backgroundColor: "white",
  tabBar: false,
};

export default ScreenContainer;

const StyledScreenContainer = styled.View`
  padding: 30px;
  flex: 1;
  /* border: solid pink 5px; */
`;

const HeaderSection = styled.View`
  /* border: 3px solid green; */
  height: 55px;
`;

const FooterSection = styled.View`
  /* border: 3px solid green; */
  /* height: 55px; */
`;

const ChildrenSection = styled.ScrollView`
  /* border: 3px solid green; */
  flex: 1;

  /* margin-bottom: 20px; */
  /* flex: 1; */
  /* height: 100%; */
`;
