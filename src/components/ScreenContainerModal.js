// react
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { StatusBar } from "expo-status-bar";
import { SafeAreaView } from "react-native-safe-area-context";

const ScreenContainerModal = ({
  backgroundColor,
  children,
  header,
  outsideModal,
  statusBarStyle,
}) => {
  return (
    <SafeAreaView
      style={{ backgroundColor, flex: 1 }}
      edges={["right", "top", "left"]}
    >
      <StyledScreenContainerModal>
        <StatusBar style={statusBarStyle || "light"}></StatusBar>
        {header && <HeaderSection>{header}</HeaderSection>}
        {outsideModal && (
          <OutsideModalSection>{outsideModal}</OutsideModalSection>
        )}
        <ChildrenSection>{children}</ChildrenSection>
      </StyledScreenContainerModal>
    </SafeAreaView>
  );
};
export default ScreenContainerModal;

const StyledScreenContainerModal = styled.View`
  height: 100%;
`;

const HeaderSection = styled.View`
  padding: 30px 30px 0 30px;
  z-index: 10;
`;

const OutsideModalSection = styled.View`
  flex: 1;
  /* margin: 20px 0 -50px 0; */
  margin-bottom: -25px;
  margin-top: 10px;
`;

const ChildrenSection = styled.View`
  background-color: white;
  padding: 20px 30px;
  padding-bottom: 30px;
  border-top-left-radius: 32px;
  border-top-right-radius: 32px;
  /* border: solid red 3px; */
`;
