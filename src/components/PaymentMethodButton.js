// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";

const PaymentMethodButton = ({
  icon,
  label,
  onPressFunc,
  selectedPaymentMethod,
}) => {
  return (
    <StyledPaymentMethodButton
      onPress={onPressFunc}
      selectedPaymentMethod={selectedPaymentMethod}
    >
      {icon}
      <Label>{label}</Label>
    </StyledPaymentMethodButton>
  );
};
export default PaymentMethodButton;

const StyledPaymentMethodButton = styled.TouchableOpacity`
  border: ${(props) =>
    props.selectedPaymentMethod === true
      ? "#027335 1px solid"
      : "#e8eff3 1px solid"};
  /* border: #e8eff3 1px solid; */
  border-radius: 12px;
  height: 110px;
  margin: 5px;
  flex: 1;
  padding: 20px;
  display: flex;
  align-items: center;
`;

const Label = styled.Text`
  font-family: Poppins-500;
  font-size: 12px;
  color: #303733;
  text-align: center;
  margin-top: 10px;
`;
