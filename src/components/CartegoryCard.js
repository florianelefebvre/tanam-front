// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, FlatList, Image } from "react-native";
import { useSelector } from "react-redux";

// logos
import BakeryLogo from "../assets/images/bakery.png";
import ChickenLogo from "../assets/images/chicken.png";
import DairyLogo from "../assets/images/dairy.png";
import FishLogo from "../assets/images/fish.png";
import FruitsLogo from "../assets/images/fruits.png";
import MushroomLogo from "../assets/images/mushroom.png";
import PizzasLogo from "../assets/images/pizzas.png";
import VegetablesLogo from "../assets/images/vegetables.png";

// data
import products from "../assets/data/products.json";

const CategoryCard = ({ size, category, onPressFunc }) => {
  const categoryIdSelected = useSelector(
    (state) => state.products.filters.categoryId
  );

  const makeCategoryProducts = () => {
    const tab = [];
    products.forEach((product) => {
      if (product.category === category.name) {
        tab.push(product);
      }
    });

    return tab;
  };

  const displayLogo = (categoryName) => {
    switch (categoryName) {
      case "Fruits":
        return <Logo source={FruitsLogo} resizeMode="contain" />;
      case "Vegetables":
        return <Logo source={VegetablesLogo} resizeMode="contain" />;
      case "Bakery":
        return <Logo source={BakeryLogo} resizeMode="contain" />;
      case "Dairy":
        return <Logo source={DairyLogo} resizeMode="contain" />;
      case "Fish":
        return <Logo source={FishLogo} resizeMode="contain" />;
      case "Mushroom":
        return <Logo source={MushroomLogo} resizeMode="contain" />;
      case "Pizzas":
        return <Logo source={PizzasLogo} resizeMode="contain" />;
      case "Chicken":
        return <Logo source={ChickenLogo} resizeMode="contain" />;
      default:
        return;
    }
  };

  const categoryProducts = makeCategoryProducts();

  return (
    <StyledCategoryCard
      color={category.color}
      size={size}
      selected={categoryIdSelected === category.id ? true : false}
      onPress={onPressFunc}
    >
      {displayLogo(category.name)}
      <CategoryName size={size}>{category.name}</CategoryName>
      <NbItems>
        {categoryProducts.length}
        {categoryProducts.length > 1 ? " Items" : " Item"}
      </NbItems>
    </StyledCategoryCard>
  );
};

export default CategoryCard;

const StyledCategoryCard = styled.TouchableOpacity`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: ${(props) => (props.size === "big" ? "20px" : "12px")};
  margin-right: ${(props) => (props.size === "big" ? "0" : "10px")};
  margin-bottom: ${(props) => (props.size === "big" ? "15px" : "0px")};

  width: ${(props) => (props.size === "big" ? "135px" : "90px")};
  height: ${(props) => (props.size === "big" ? "140px" : "90px")};
  background-color: ${(props) => props.color};
  /* border: ${(props) => `${props.color} solid 1px`}; */
`;

const Logo = styled.Image`
  width: 25%;
  height: 25%;
  margin-bottom: ${(props) => (props.size === "big" ? "30px" : "5px")};
`;

const CategoryName = styled.Text`
  color: white;
  font-size: ${(props) => (props.size === "big" ? "18px" : "14px")};
  font-family: Poppins-700;
`;

const NbItems = styled.Text`
  color: white;
  font-size: ${(props) => (props.size === "big" ? "18px" : "12px")};
  font-family: Lato-400;
`;
