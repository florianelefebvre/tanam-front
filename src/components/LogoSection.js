// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import TanamLogo from "../assets/images/tanam.png";

const LogoSection = ({}) => {
  return (
    <StyledLogoSection>
      <Logo source={TanamLogo}></Logo>
      <Subtitle>Grocery App</Subtitle>
    </StyledLogoSection>
  );
};
export default LogoSection;

const StyledLogoSection = styled.View`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.Image`
  width: 125px;
  height: 36px;
  margin: 15px;
`;

export const Subtitle = styled.Text`
  color: white;
  font-family: Lato-400;
  font-size: 16px;
`;
