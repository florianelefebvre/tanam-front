// react
import React, { useEffect } from "react";
import { TouchableOpacity, View, Text } from "react-native";
import { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import styled from "styled-components/native";

// icons
import icons from "../assets/icons";
import { Entypo } from "@expo/vector-icons";

// colors
import { green } from "../assets/colors.json";

const { filterIconGrey } = icons;

const CustomedInputFormValidation = ({
  placeholder,
  icon,
  height,
  width,
  secureTextEntry,
  filter,
  onPressFilterIconFunc,
  controllerName,
  control,
  error,
  errorMessage,
  patternValue,
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const [hidePassword, setHidePassword] = useState(secureTextEntry);

  return (
    <View>
      <StyledInput width={width} height={height} isFocused={isFocused}>
        <IconAndText>
          {icon && <Text style={{ marginRight: 20 }}>{icon}</Text>}

          <Controller
            control={control}
            name={controllerName}
            rules={{
              required: {
                value: true,
              },
              pattern: {
                value: patternValue || "",
              },
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                onFocus={() => {
                  setIsFocused(true);
                }}
                selectionColor={green}
                onChangeText={onChange}
                placeholder={placeholder}
                secureTextEntry={hidePassword}
                value={value}
                onBlur={onBlur}
              ></TextInput>
            )}
          />
        </IconAndText>

        {secureTextEntry === true && (
          <TouchableOpacity
            onPress={() => {
              setHidePassword(!hidePassword);
            }}
          >
            {hidePassword ? (
              <Entypo name="eye-with-line" size={24} color="#027335" />
            ) : (
              <Entypo name="eye" size={24} color="#027335" />
            )}
          </TouchableOpacity>
        )}
        {filter === true && (
          <TouchableOpacity onPress={onPressFilterIconFunc}>
            {filterIconGrey}
          </TouchableOpacity>
        )}
      </StyledInput>
      {error && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </View>
  );
};

export default CustomedInputFormValidation;

export const StyledInput = styled.View`
  padding: 15px;
  background-color: #e8eff3;
  font-size: 16px;
  height: ${({ height }) => height || "60px"};
  width: ${(props) => props.width || "100%"};
  border-radius: 14px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border: ${({ isFocused }) => (isFocused ? "solid #027335 1px" : "none")};
  margin-right: 10px;
`;

export const IconAndText = styled.View`
  display: flex;
  flex-direction: row;
  max-width: 80%;
`;

export const ErrorMessage = styled.Text`
  color: red;
  text-align: center;
  margin: 10px;
`;

export const TextInput = styled.TextInput`
  color: #293041;
  font-family: Lato-400;
  font-size: 16px;
  margin-left: 5px;
  overflow: hidden;
  width: 100%;
`;
