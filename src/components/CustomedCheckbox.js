// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { CheckBox } from "react-native-elements";

const CustomedCheckBox = ({ label, checkMark, setCheckMark }) => {
  return (
    <StyledCustomedCheckBox>
      <CheckBoxWrap>
        <CheckBox
          checked={checkMark}
          checkedColor="#027335"
          onPress={setCheckMark}
          size={30}
        ></CheckBox>
      </CheckBoxWrap>
      <Label>{label}</Label>
    </StyledCustomedCheckBox>
  );
};
export default CustomedCheckBox;

const StyledCustomedCheckBox = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 50%;
  margin: 10px 0px;
`;
const CheckBoxWrap = styled.View`
  margin: -20px;
  margin-right: -10px;
`;

const Label = styled.Text`
  font-family: Lato-400;
  font-size: 16px;
  color: #293041;
`;
