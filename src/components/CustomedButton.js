import React from "react";
import styled from "styled-components/native";
import { Feather } from "@expo/vector-icons";

const CustomedButton = ({
  title,
  theme,
  shoppingCart,
  onPressFunc,
  disabled,
}) => {
  return (
    <StyledButton theme={theme} onPress={onPressFunc} disabled={disabled}>
      {shoppingCart && <Feather name="shopping-cart" size={24} color="white" />}
      <StyledText theme={theme} shoppingCart={shoppingCart}>
        {title}
      </StyledText>
    </StyledButton>
  );
};

export default CustomedButton;

const StyledButton = styled.TouchableOpacity`
  background-color: ${(props) =>
    props.theme === "dark" ? "#027335" : "white"};
  /* margin-top: 0px; */
  width: 100%;
  height: 60px;
  border-radius: 12px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: ${(props) => (props.theme === "dark" ? "none" : "#027335")};
`;

const StyledText = styled.Text`
  color: ${(props) => (props.theme === "dark" ? "white" : "#027335")};
  font-family: Poppins-800;
  margin-left: ${(props) => (props.shoppingCart ? "17px" : "0")};
`;
