// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useState, useEffect } from "react";
import { WBS_URL } from "../utils/globalVars";

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  removeProductFromCart,
  addProductToCart,
} from "../redux/actions/shoppingCart.actions";

// components
import Counter from "../components/Counter";

// styled components
import { Line } from "../styles/global.styles";

const ShoppingCartProductCard = ({ product }) => {
  const dispatch = useDispatch();

  const [quantity, setQuantity] = useState(0);

  const shoppingCart = useSelector((state) => state.shoppingCart.cart);

  useEffect(() => {
    console.log("-----------product----------");
    console.log(product);
    console.log("---------------------");
    const findQuantityProduct = (productId) => {
      let productNotFound = true;
      shoppingCart.forEach((productInShoppingCart) => {
        if (productInShoppingCart.id === productId) {
          setQuantity(product.quantity);
          productNotFound = false;
        }
      });
      if (productNotFound) {
        setQuantity(0);
      }
    };
    findQuantityProduct(product.id);
  }, [shoppingCart]);

  return (
    <StyledShoppingCartProductCard>
      <ImageView
        source={{
          uri: product.img_url && `${WBS_URL}${product.img_url}`,
        }}
      ></ImageView>
      <ProductCartDetails>
        <ProductName>{product.name}</ProductName>
        <BottomViewProduct>
          <ProductPrice> $ {product.price}</ProductPrice>
          <CounterWrap>
            <Counter
              theme="light"
              product={product}
              quantity={quantity}
              incFunc={() => {
                dispatch(addProductToCart(product));
              }}
              decFunc={() => {
                dispatch(removeProductFromCart(product));
              }}
            ></Counter>
          </CounterWrap>
        </BottomViewProduct>
      </ProductCartDetails>

      <Line></Line>
    </StyledShoppingCartProductCard>
  );
};
export default ShoppingCartProductCard;

const StyledShoppingCartProductCard = styled.View`
  display: flex;
  flex-direction: row;
  padding: 10px 0;
  align-items: center;
`;

const ProductCartDetails = styled.View`
  flex: 3;
  margin-left: 20px;
`;

const ImageView = styled.Image`
  background-color: #c4c4c4;
  border-radius: 12px;
  flex: 1;
  height: 100%;
`;

const ProductName = styled.Text`
  font-family: Lato-700;
  font-size: 18px;
`;

const ProductPrice = styled.Text`
  font-family: Lato-700;
  font-size: 16px;
  color: #293041;
  width: 45%;
`;

const CounterWrap = styled.View`
  flex: 1;
`;

const BottomViewProduct = styled.View`
  margin-top: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;
