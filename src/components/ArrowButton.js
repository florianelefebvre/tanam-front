// react
import React from "react";
import { useState, useEffect } from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";

// icons
import icons from "../assets/icons";
const { arrowDownIcon, arrowUpIcon } = icons;

const ArrowButton = ({ orders, setOrders, index }) => {
  return (
    <StyledArrowButton
      onPress={() => {
        const newState = [...orders];
        newState[index].showDetails = !newState[index].showDetails;
        setOrders(newState);
      }}
    >
      {orders[index].showDetails ? arrowUpIcon : arrowDownIcon}
    </StyledArrowButton>
  );
};
export default ArrowButton;

const StyledArrowButton = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
`;
