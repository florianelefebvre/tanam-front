// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";

const ScreenContainer = ({
  children,
  statusBarStyle,
  header,
  backgroundColor,
  tabBar,
  childMinHeight,
}) => {
  return (
    <SafeAreaView
      style={{ backgroundColor, flex: 1 }}
      edges={tabBar ? ["right", "top", "left"] : undefined}
    >
      <StyledScreenContainer>
        <StatusBar style={statusBarStyle}></StatusBar>
        <HeaderSection>{header}</HeaderSection>
        <ChildrenSection childMinHeight={childMinHeight}>
          {children}
        </ChildrenSection>
      </StyledScreenContainer>
    </SafeAreaView>
  );
};

ScreenContainer.defaultProps = {
  statusBarStyle: "dark",
  backgroundColor: "white",
  tabBar: false,
};

export default ScreenContainer;

const StyledScreenContainer = styled.ScrollView`
  padding: 30px;
  display: flex;
`;

const HeaderSection = styled.View`
  /* border: 3px solid green; */
  height: 55px;
`;

const ChildrenSection = styled.View`
  /* border: 3px solid green; */
  min-height: ${(props) => props.childMinHeight || 0};
  /* display: flex; */
  /* justify-content: space-between; */
  /* min-height: 100%; */
`;
