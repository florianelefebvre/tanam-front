// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, Image } from "react-native";
import { useState, useEffect } from "react";
import { WBS_URL } from "../utils/globalVars";

// icons
import icons from "../assets/icons";
const { fullStarIcon, filledHeartIcon, clearHeartIcon } = icons;

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  removeProductFromCart,
  addProductToCart,
} from "../redux/actions/shoppingCart.actions";

// components
import Counter from "../components/Counter";

const PopularDealCard = ({ product, onPressFunc }) => {
  const [liked, setLiked] = useState(false);
  const [quantity, setQuantity] = useState(0);
  const [displayCounter, setDisplayCounter] = useState(false);
  const shoppingCart = useSelector((state) => state.shoppingCart.cart);

  const dispatch = useDispatch();

  useEffect(() => {
    const findQuantityProduct = (productId) => {
      let productNotFound = true;
      shoppingCart.forEach((productInShoppingCart) => {
        if (productInShoppingCart.id === productId) {
          setQuantity(productInShoppingCart.quantity);
          productNotFound = false;
        }
      });
      if (productNotFound) {
        setQuantity(0);
      }
    };
    findQuantityProduct(product.id);
  }, [shoppingCart]);

  return (
    <StyledPopularDealCard onPress={onPressFunc}>
      <ImageView
        style={{ flex: 1 }}
        source={{
          uri: product.img_url
            ? `${WBS_URL}${product.img_url}`
            : "https://media.istockphoto.com/photos/fresh-fruit-display-in-open-food-market-in-rome-italy-picture-id492262592",
        }}
      ></ImageView>
      <Heart
        onPress={() => {
          setLiked(!liked);
        }}
      >
        {liked ? (
          <Text> {filledHeartIcon}</Text>
        ) : (
          <Text> {clearHeartIcon}</Text>
        )}
      </Heart>

      <DetailsView>
        <DealTitle>{product.name}</DealTitle>
        <PriceAndStarSection>
          <Price>
            ${" "}
            {product.priceAfterDiscount
              ? product.priceAfterDiscount
              : product.price}
          </Price>
          <RateView>
            <Rate>(243)</Rate>
            {fullStarIcon}
          </RateView>
        </PriceAndStarSection>
        <CounterSection>
          {quantity ? (
            <Counter
              setDisplayCounter={setDisplayCounter}
              product={product}
              quantity={quantity}
              incFunc={() => {
                dispatch(addProductToCart(product));
              }}
              decFunc={() => {
                dispatch(removeProductFromCart(product));
              }}
            ></Counter>
          ) : (
            <Button
              onPress={() => {
                setDisplayCounter(!displayCounter);
                dispatch(addProductToCart(product));
              }}
            >
              <Label>Add to Cart</Label>
            </Button>
          )}
        </CounterSection>
      </DetailsView>
    </StyledPopularDealCard>
  );
};

export default PopularDealCard;

const StyledPopularDealCard = styled.TouchableOpacity`
  border: #e8eff3 solid 1px;
  width: 47%;
  margin-bottom: 20px;
  height: 310px;
  border-radius: 12px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const ImageView = styled.Image`
  height: 45%;
  background-color: white;
  border-radius: 12px;
`;

// const Image = styled.Image``;

const DetailsView = styled.View``;

const DealTitle = styled.Text`
  color: #303733;
  font-size: 16px;
  font-family: Lato-700;
  text-align: center;
  padding: 10px;
`;
const PriceAndStarSection = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 10px 30px;
`;

const Price = styled.Text`
  color: #c29c1d;
  font-family: Poppins-500;
  font-size: 14px;
`;

const RateView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Rate = styled.Text`
  color: #7d8fab;
  font-family: Lato-400;
  font-size: 10px;
  margin-right: 5px;
`;

const CounterSection = styled.View`
  margin: 10px 20px;
`;

const Button = styled.TouchableOpacity`
  background-color: #c8edd9;
  height: 36px;
  border-radius: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  color: #027335;
  font-family: Lato-700;
  font-size: 14px;
`;

const Heart = styled.TouchableOpacity`
  position: absolute;
  top: 15px;
  left: 15px;
`;
