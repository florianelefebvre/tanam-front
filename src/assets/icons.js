import React from "react";
import {
  Ionicons,
  Fontisto,
  AntDesign,
  MaterialIcons,
  FontAwesome,
  Feather,
  Entypo,
  FontAwesome5,
  MaterialCommunityIcons,
  SimpleLineIcons,
} from "@expo/vector-icons";

import { grey } from "./colors.json";

export default {
  loginIcon: <Ionicons name="person" size={24} color={grey} />,
  passwordIcon: <Fontisto name="locked" size={24} color={grey} />,
  mailIcon: <Entypo name="mail" size={24} color={grey} />,
  searchIcon: <AntDesign name="search1" size={24} color={grey} />,
  fullStarIcon: <MaterialIcons name="star" size={24} color="#FFA902" />,
  clearStarIcon: <MaterialIcons name="star-border" size={24} color="#FFA902" />,
  filledHeartIcon: <AntDesign name="heart" size={24} color="#FF6464" />,
  filledHeartWhiteIcon: <AntDesign name="heart" size={24} color="white" />,
  clearHeartIcon: <AntDesign name="hearto" size={24} color="white" />,
  crossIcon: <Ionicons name="close" size={35} color="black" />,
  arrowRightIcon: <AntDesign name="right" size={20} color="#7D8FAB" />,
  arrowDownIcon: (
    <SimpleLineIcons name="arrow-down" size={15} color="#7D8FAB" />
  ),
  arrowUpIcon: <SimpleLineIcons name="arrow-up" size={15} color="#7D8FAB" />,
  filterIconGrey: <FontAwesome name="filter" size={24} color="#7D8FAB" />,
  filterIconBlack: <FontAwesome name="filter" size={24} color="black" />,
  shoppingCartIcon: <Feather name="shopping-cart" size={24} color="white" />,
  goBackArrowBlackIcon: (
    <FontAwesome5 name="arrow-left" size={24} color="black" />
  ),
  goBackArrowWhiteIcon: (
    <FontAwesome5 name="arrow-left" size={24} color="white" />
  ),
  //tabbar
  home: <Entypo name="home" size={24} color="#7D8FAB" />,
  cashOnDelivGreyIcon: (
    <MaterialCommunityIcons name="account-cash" size={24} color="#7D8FAB" />
  ),
  cashOnDelivGreenIcon: (
    <MaterialCommunityIcons name="account-cash" size={24} color="#027335" />
  ),
  creditCardGreyIcon: (
    <FontAwesome name="credit-card" size={24} color="#7D8FAB" />
  ),
  creditCardGreenIcon: (
    <FontAwesome name="credit-card" size={24} color="#027335" />
  ),

  paypalGreyIcon: <SimpleLineIcons name="paypal" size={24} color="#7D8FAB" />,
  paypalGreenIcon: <SimpleLineIcons name="paypal" size={24} color="#027335" />,
  searchIcon: <AntDesign name="search1" size={24} color="#7D8FAB" />,
  packageIcon: <Feather name="package" size={30} color="white" />,

  phoneIcon: <MaterialCommunityIcons name="phone" size={30} color="#027335" />,
  chatIcon: <Ionicons name="chatbubble-ellipses" size={30} color="#027335" />,
  checkmarkCircleIcon: (
    <Ionicons name="checkmark-circle" size={18} color="#027335" />
  ),
  logOutIcon: <MaterialIcons name="logout" size={24} color="black" />,

  discountIcon: <Ionicons name="pricetag" size={20} color="#C29C1D" />,
  deliveryIcon: (
    <MaterialCommunityIcons name="truck-delivery" size={20} color="#027335" />
  ),
};
