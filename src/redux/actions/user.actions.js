export const types = {
  SET_USER: "user/SET_USER",
  SET_LOGOUT: "user/SET_LOGOUT",
  SET_SHIPPING_DETAILS: "user/SET_SHIPPING_DETAILS",
  // SET_PAYMENT_DETAILS: "user/SET_PAYMENT_DETAILS",
  SET_PAYMENT_METHOD: "user/SET_PAYMENT_METHOD",
};

export const setUser = (payload) => {
  return {
    type: types.SET_USER,
    payload,
  };
};

export const setLogout = (payload) => {
  return {
    type: types.SET_LOGOUT,
    payload,
  };
};

export const setShippingDetails = (payload) => {
  return {
    type: types.SET_SHIPPING_DETAILS,
    payload,
  };
};

// export const setPaymentDetails = (payload) => {
//   return {
//     type: types.SET_PAYMENT_METHOD,
//     payload,
//   };
// };

export const setPaymentMethod = (payload) => {
  return {
    type: types.SET_PAYMENT_METHOD,
    payload,
  };
};
