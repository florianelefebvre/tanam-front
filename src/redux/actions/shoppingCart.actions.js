export const types = {
  ADD_PRODUCT: "shoppingCart/ADD_PRODUCT",
  REMOVE_PRODUCT: "shoppingCart/REMOVE_PRODUCT",
  SET_CART_PRICE: "shoppingCart/SET_CART_PRICE",
  RESET_SHOPPING_CART: "shoppingCart/RESET_SHOPPING_CART",
};

export const addProductToCart = (payload) => {
  return {
    type: types.ADD_PRODUCT,
    payload,
  };
};
export const removeProductFromCart = (payload) => {
  return {
    type: types.REMOVE_PRODUCT,
    payload,
  };
};

export const setCartPrice = (payload) => {
  return {
    type: types.SET_CART_PRICE,
    payload,
  };
};

export const resetShoppingCart = (payload) => {
  return {
    type: types.RESET_SHOPPING_CART,
    payload,
  };
};
