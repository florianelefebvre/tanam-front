export const types = {
  SET_FILTERS: "product/SET_FILTERS",
  RESET_FILTERS: "products/RESET_FILTERS",
  SET_PRODUCTS_TO_DISPLAY: "products/SET_PRODUCTS_TO_DISPLAY",
  SET_CATEGORIES: "products/SET_CATEGORIES",
};

export const setFiltersProducts = (payload) => {
  return {
    type: types.SET_FILTERS,
    payload,
  };
};

export const resetFiltersProducts = (payload) => {
  return {
    type: types.RESET_FILTERS,
    payload,
  };
};

export const setProductsToDisplay = (payload) => {
  return {
    type: types.SET_PRODUCTS_TO_DISPLAY,
    payload,
  };
};

export const setCategories = (payload) => {
  return {
    type: types.SET_CATEGORIES,
    payload,
  };
};
