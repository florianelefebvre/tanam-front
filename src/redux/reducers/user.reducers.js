import { ActionSheetIOS } from "react-native";
import { types as typesUser } from "../actions/user.actions";

const initialState = {
  userName: "",
  userToken: null,
  shippingDetails: {
    fullName: "",
    email: "",
    phone: "",
    address: "",
    zipCode: "",
    city: "",
    country: "",
  },
  paymentDetails: {
    paymentMethod: {
      cashOnDelivery: false,
      creditCard: true,
      paypalMethod: false,
    },
    cardHolderName: "",
    cardNumber: "",
    cardMonthYear: "",
    cardCvv: "",
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case typesUser.SET_USER:
      return {
        ...state,
        userToken: action.payload.userToken,
        userName: action.payload.userName,
      };

    case typesUser.SET_LOGOUT:
      return {
        ...state,
        userToken: null,
        userName: "",
      };

    case typesUser.SET_SHIPPING_DETAILS:
      return {
        ...state,
        shippingDetails: action.payload.formData,
      };

    case typesUser.SET_PAYMENT_METHOD:
      const newPaymentDetails = state.paymentDetails;

      newPaymentDetails.paymentMethod = action.payload;

      return {
        ...state,
        paymentDetails: newPaymentDetails,
      };
    default:
      return state;
  }
};

export default reducer;
