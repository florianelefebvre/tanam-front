import { types as typesOrders } from "../actions/shoppingCart.actions";

const initialState = {
  cart: [],
  subtotal: 0,
  taxe: 0,
  total: 0,
};

const reducer = (state = initialState, action) => {
  let newCart = [...state.cart];
  switch (action.type) {
    case typesOrders.ADD_PRODUCT:
      let productNotFound = true;

      newCart.forEach((product) => {
        if (product.id === action.payload.id) {
          product.quantity += 1;
          productNotFound = false;
          return { ...state, cart: newCart };
        }
      });
      if (productNotFound) {
        let newProduct = action.payload;
        newProduct.quantity = 1;
        newCart.push(newProduct);
      }
      return { ...state, cart: newCart };

    case typesOrders.REMOVE_PRODUCT:
      let product = action.payload;

      for (let i = 0; newCart[i]; i++) {
        if (product.id === action.payload.id) {
          if (product.quantity === 1) {
            newCart.splice(i, 1);
          } else product.quantity -= 1;
          return { ...state, cart: newCart };
        }
      }

      return { ...state, cart: newCart };

    case typesOrders.SET_CART_PRICE:
      let subtotalCart = state.cart.map((product) => {
        if (product.priceAfterDiscount)
          return product.priceAfterDiscount * product.quantity;
        else return product.price * product.quantity;
      });
      subtotalCart = subtotalCart.reduce((prev, curr) => prev + curr, 0);

      return {
        ...state,
        subtotal: subtotalCart,
        taxe: subtotalCart * 0.2,
        total: subtotalCart * 0.8,
      };

    case typesOrders.RESET_SHOPPING_CART:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
