import { combineReducers } from "redux";
import products from "./products.reducers";
import shoppingCart from "./shoppingCart.reducers";
import user from "./user.reducers";

export default combineReducers({
  products,
  shoppingCart,
  user,
});
