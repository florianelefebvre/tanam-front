import { types as typesProducts } from "../actions/products.actions";

const initialState = {
  filters: {
    priceMin: 0,
    priceMax: 100,
    minRate: 0,
    discount: false,
    voucherAccepted: false,
    freeShipping: false,
    sameDayDeliv: false,
    categoryId: "",
    inputSearch: "",
    popular: false,
  },
  productsToDisplay: [],
  categories: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case typesProducts.SET_FILTERS:
      let filters = { ...state.filters };

      let keysToModify = Object.keys(action.payload);

      for (let i = 0; i < keysToModify.length; i++) {
        Object.defineProperty(filters, keysToModify[i], {
          value: action.payload[keysToModify[i]],
        });
      }
      return { ...state, filters };

    case typesProducts.RESET_FILTERS:
      return { ...state, filters: { ...initialState.filters, popular: false } };

    case typesProducts.SET_PRODUCTS_TO_DISPLAY:
      return { ...state, productsToDisplay: action.payload.productsToDisplay };

    case typesProducts.SET_CATEGORIES:
      return { ...state, categories: action.payload };

    default:
      return state;
  }
};

export default reducer;
