export const REGEXP = {
  phone: /^(0|\+33)[1-9]([-. ]?[0-9]{2}){4}$/,
  numeric: /^[0-9]*$/,
  alpha: /^[ a-zA-Z]+$/,
  address: /^[ ,a-zA-Z0-9]+$/,
  email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/,
  creditCardNumber: /^[0-9]{16}$/,
  creditCardMonthYear: /^[0-9]{2}\/[0-9]{2}$/,
  creditCardCvv: /^[0-9]{3}$/,
};

// export const WBS_URL = "https://5a7c-45-15-205-254.ngrok.io";
export const WBS_URL = "https://tanam-back.herokuapp.com";
// <Image source={`${WBS_URL}${product.img_url}`}></Image>
