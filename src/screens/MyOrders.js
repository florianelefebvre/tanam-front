// react
import React from "react";
import { useState, useEffect } from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import axios from "axios";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import { WBS_URL } from "../utils/globalVars";

// components
import ScreenContainer from "../components/ScreenContainer";
import ArrowButton from "../components/ArrowButton";
import OrderStatusCard from "../components/OrderStatusCard";

// styled components
import { H1, Lato70016, Line } from "../styles/global.styles";

// icons
import icons from "../assets/icons";

const { searchIcon, packageIcon } = icons;

const MyOrders = ({}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [orders, setOrders] = useState();

  const userToken = useSelector((state) => state.user.userToken);

  const [ordersFilters, setOrdersFilters] = useState({
    all: true,
    onDelivery: false,
    completed: false,
  });

  useFocusEffect(
    React.useCallback(() => {
      const fetchData = async () => {
        try {
          const url = `${WBS_URL}/orders/getOrdersByUserId`;
          const body = { userToken, ordersFilters: ordersFilters };
          const response = await axios.post(url, body);
          response.data.map((order) => {
            order.showDetails = false;
            return order;
          });
          setOrders(response.data);
          setIsLoading(false);
        } catch (error) {
          console.log({ MyOrdersError: error.message, error: error });
        }
      };
      fetchData();
      return;
    }, [ordersFilters])
  );

  const header = (
    <Header>
      <H1>My Orders</H1>
      {searchIcon}
    </Header>
  );
  return (
    !isLoading && (
      <ScreenContainer header={header} tabBar={true}>
        <StyledMyOrders>
          <StyledMyOrdersFilters>
            <FilterView
              flex={1}
              greenBorder={ordersFilters.all}
              onPress={() => {
                setOrdersFilters({
                  all: true,
                  onDelivery: false,
                  completed: false,
                });
              }}
            >
              <Title>All</Title>
            </FilterView>
            <FilterView
              flex={3}
              greenBorder={ordersFilters.onDelivery}
              onPress={() => {
                setOrdersFilters({
                  all: false,
                  onDelivery: true,
                  completed: false,
                });
              }}
            >
              <Point color={"#FFA902"}></Point>
              <Title>On Delivery</Title>
            </FilterView>
            <FilterView
              flex={3}
              greenBorder={ordersFilters.completed}
              onPress={() => {
                setOrdersFilters({
                  all: false,
                  onDelivery: false,
                  completed: true,
                });
              }}
            >
              <Point color={"#027335"}></Point>
              <Title>Completed</Title>
            </FilterView>
          </StyledMyOrdersFilters>

          {orders.map((order, index) => {
            return (
              <View key={index}>
                <OrderAndDetailsView>
                  <OrderView>
                    <PackageIconView
                      color={order.status === "Canceled" ? "red" : "#027335"}
                    >
                      {packageIcon}
                    </PackageIconView>
                    <MiddleView>
                      <Lato70016>Order ID #00123{order.id}</Lato70016>
                      <Subtitle>
                        {order.nb_products} Items • {order.status}
                      </Subtitle>
                    </MiddleView>
                    <ArrowButton
                      orders={orders}
                      setOrders={setOrders}
                      index={index}
                    />
                  </OrderView>
                  {order.showDetails && (
                    <OrderDetails>
                      <OrderStatusCard
                        title={"Order Placed"}
                        date={order.created_at}
                      ></OrderStatusCard>
                      <OrderStatusCard
                        title={"Order Confirmed"}
                        date={order.confirmed_at}
                      ></OrderStatusCard>
                      <OrderStatusCard
                        title={`Your Order On Delivery by ${order.delivery_person}`}
                        date={order.on_delivery_at}
                      ></OrderStatusCard>
                      <OrderStatusCard
                        title={"Order Delivered"}
                        date={order.delivered_at}
                      ></OrderStatusCard>
                    </OrderDetails>
                  )}
                </OrderAndDetailsView>
                <Line></Line>
              </View>
            );
          })}
        </StyledMyOrders>
      </ScreenContainer>
    )
  );
};
export default MyOrders;

const StyledMyOrders = styled.View``;

const StyledMyOrdersFilters = styled.View`
  display: flex;
  flex-direction: row;
  margin: 10px 0;
`;

const FilterView = styled.TouchableOpacity`
  flex: ${(props) => props.flex || ""};
  margin: 5px;
  border-bottom-color: ${(props) =>
    props.greenBorder ? "#027335" : "#E8EFF3"};
  border-bottom-width: 3px;
  padding: 0 5px 20px 5px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Point = styled.View`
  background-color: ${(props) => props.color || ""};
  width: 12px;
  height: 12px;
  border-radius: 10px;
  margin-right: 8px;
`;

const Title = styled.Text`
  font-family: Lato-400;
  font-size: 16px;
  color: #303733;
`;

const Subtitle = styled.Text`
  font-family: Lato-400;
  font-size: 15px;
  color: #7d8fab;
  margin-top: 8px;
`;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const OrderAndDetailsView = styled.View`
  padding: 20px 0;
`;

const OrderView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const PackageIconView = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.color || "green"};
  border-radius: 12px;
  height: 49px;
  width: 49px;
  flex: 1;
`;

const MiddleView = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-left: 20px;
  flex: 4;
`;

const OrderDetails = styled.View`
  display: flex;
  flex-direction: column;
  padding-top: 20px;
  margin-left: 10px;
`;
