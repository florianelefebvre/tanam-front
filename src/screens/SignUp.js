// react
import React, { useState } from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { useDispatch } from "react-redux";
import axios from "axios";
import { useForm } from "react-hook-form";
import { WBS_URL } from "../utils/globalVars";

// components
import CustomedInputFormValidation from "../components/CustomedInputFormValidation";
import CustomedButton from "../components/CustomedButton";
import ScreenContainerModal from "../components/ScreenContainerModal";
import LogoSection from "../components/LogoSection";

// utils
import { grey } from "../assets/colors.json";
import { H1, Margin10 } from "../styles/global.styles";
import { REGEXP } from "../utils/globalVars";
import icons from "../assets/icons";

// redux
import { setUser } from "../redux/actions/user.actions";

const { loginIcon, passwordIcon, mailIcon } = icons;

const SignUp = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    defaultValues: { fullName: "", email: "", password: "" },
  });

  const onSubmit = (formData) => {
    const signUpUser = async () => {
      try {
        const url = `${WBS_URL}/signUp`;
        const payload = {
          fullName: formData.fullName,
          email: formData.email,
          password: formData.password,
        };
        const response = await axios.post(url, payload);
        if (response.data) {
          dispatch(
            setUser({ userName: formData.fullName, userToken: response.data })
          );
        }
      } catch (error) {
        if (error.response.data.errorMessage)
          alert(error.response.data.errorMessage);
        console.log("error");
      }
    };
    signUpUser();
    return;
  };

  const outsideModal = <LogoSection />;

  return (
    <ScreenContainerModal backgroundColor="#027335" outsideModal={outsideModal}>
      <Margin10>
        <H1>Create your account</H1>
      </Margin10>
      <Margin10>
        <Subtitle color={grey} fontSize="14px">
          Lorem ipsum dolor sit amet
        </Subtitle>
      </Margin10>

      <View style={{ marginTop: 15, marginBottom: 15 }}>
        <CustomedInputFormValidation
          placeholder="Louis Anderson"
          icon={loginIcon}
          height={"60px"}
          width={"100%"}
          controllerName="fullName"
          control={control}
          error={errors.fullName}
          errorMessage={"This Field is required"}
          patternValue={REGEXP.alpha}
        ></CustomedInputFormValidation>
        <View style={{ height: 10 }}></View>
        <CustomedInputFormValidation
          placeholder="louisanderson@gmail.com"
          icon={mailIcon}
          height={"60px"}
          width={"100%"}
          controllerName="email"
          control={control}
          error={errors.email}
          errorMessage={"Please enter a correct email adress"}
          patternValue={REGEXP.email}
        ></CustomedInputFormValidation>
        <View style={{ height: 10 }}></View>
        <CustomedInputFormValidation
          placeholder="password"
          icon={passwordIcon}
          height={"60px"}
          width={"100%"}
          secureTextEntry={true}
          controllerName="password"
          control={control}
          error={errors.password}
          errorMessage={"Please enter a password"}
        ></CustomedInputFormValidation>
      </View>

      <CustomedButton
        title="REGISTER"
        theme="dark"
        width="100%"
        disabled={!isValid}
        onPressFunc={handleSubmit(onSubmit)}
      ></CustomedButton>
      <AcceptTermsView>
        <KeepSignInView>
          <Subtitle style={{ marginLeft: 10 }}>
            By tapping "Register" you accept our{" "}
            <Text style={{ color: "#027335" }}>terms</Text> and
            <Text style={{ color: "#027335" }}> condition</Text>
          </Subtitle>
        </KeepSignInView>
      </AcceptTermsView>

      <Subtitle style={{ marginBottom: 15, textAlign: "center" }}>
        Already have an account?
      </Subtitle>
      <CustomedButton
        title="SIGN IN"
        theme="light"
        onPressFunc={() => {
          navigation.navigate("SignIn");
        }}
      ></CustomedButton>
    </ScreenContainerModal>
  );
};

export default SignUp;

const KeepSignInView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 0;
  max-width: 100%;
`;

const AcceptTermsView = styled.View`
  font-size: 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 15px 0;
`;
export const Subtitle = styled.Text`
  color: ${(props) => props.color || "#7D8FAB"};
  font-family: Lato-400;
  font-size: ${(props) => props.fontSize || "16px"};
`;
