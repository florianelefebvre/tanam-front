// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { Rating } from "react-native-ratings";
import { useNavigation } from "@react-navigation/native";

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  setFiltersProducts,
  resetFiltersProducts,
} from "../redux/actions/products.actions";

// components
import CustomedInputBasic from "../components/CustomedInputBasic";
import CustomedButton from "../components/CustomedButton";
import ScreenContainerBasic from "../components/ScreenContainerBasic";
import CustomedCheckBox from "../components/CustomedCheckbox";

// styles
import { H1, H2, Line } from "../styles/global.styles";

const SearchFilters = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // checkbox filters
  const discount = useSelector((state) => state.products.filters.discount);
  const voucherAccepted = useSelector(
    (state) => state.products.filters.voucherAccepted
  );
  const freeShipping = useSelector(
    (state) => state.products.filters.freeShipping
  );
  const sameDayDeliv = useSelector(
    (state) => state.products.filters.sameDayDeliv
  );
  const categories = useSelector((state) => state.products.categories);

  // other filters
  const minRate = useSelector((state) => state.products.filters.minRate);
  const priceMin = useSelector((state) => state.products.filters.priceMin);
  const priceMax = useSelector((state) => state.products.filters.priceMax);

  const ratingCompleted = (minRate) => {
    dispatch(
      setFiltersProducts({
        minRate,
      })
    );
  };

  const header = (
    <Header>
      <TitleSection>
        <H1>Search Filters</H1>
      </TitleSection>
      <Reset
        onPress={() => {
          dispatch(resetFiltersProducts());
          navigation.goBack();
        }}
      >
        <ResetText>Reset</ResetText>
      </Reset>
    </Header>
  );

  const footer = (
    <CustomedButton
      title={"APPLY"}
      theme={"dark"}
      onPressFunc={() => {
        dispatch(setFiltersProducts({ inputSearch: "" }));
        navigation.goBack();
      }}
    ></CustomedButton>
  );

  return (
    <ScreenContainerBasic statusBarStyle="dark" header={header} footer={footer}>
      <StyledSearchFilters>
        <Section>
          <H2>Price Range</H2>
          <InputsSection>
            <CustomedInputBasic
              placeholder={"Min"}
              width={"150px"}
              height={"60px"}
              value={`${priceMin}`}
              onChangeFunc={(text) => {
                dispatch(
                  setFiltersProducts({
                    priceMin: text,
                  })
                );
              }}
            ></CustomedInputBasic>
            <CustomedInputBasic
              placeholder={"Max"}
              width={"150px"}
              height={"60px"}
              value={`${priceMax}`}
              onChangeFunc={(text) => {
                dispatch(
                  setFiltersProducts({
                    priceMax: text,
                  })
                );
              }}
            ></CustomedInputBasic>
          </InputsSection>
        </Section>

        <Line></Line>
        <Section>
          <H2>Star Ratings</H2>
          <StarsSection>
            <Rating
              type="custom"
              ratingColor="#FFA902"
              ratingBackgroundColor="lightgrey"
              ratingCount={5}
              imageSize={30}
              tintColor="white"
              onFinishRating={ratingCompleted}
              startingValue={minRate}
            ></Rating>
            <NumberStars>{minRate} Stars</NumberStars>
          </StarsSection>
        </Section>

        <Line></Line>
        <Section>
          <H2>Others</H2>
          <OthersSection>
            <CustomedCheckBox
              label={"Discount"}
              checkMark={discount}
              setCheckMark={() => {
                dispatch(setFiltersProducts({ discount: !discount }));
              }}
            ></CustomedCheckBox>
            <CustomedCheckBox
              label={"Voucher"}
              checkMark={voucherAccepted}
              setCheckMark={() => {
                dispatch(
                  setFiltersProducts({ voucherAccepted: !voucherAccepted })
                );
              }}
            ></CustomedCheckBox>
            <CustomedCheckBox
              label={"Free Shipping"}
              checkMark={freeShipping}
              setCheckMark={() => {
                dispatch(setFiltersProducts({ freeShipping: !freeShipping }));
              }}
            ></CustomedCheckBox>
            <CustomedCheckBox
              label={"Same Day Deliv."}
              checkMark={sameDayDeliv}
              setCheckMark={() => {
                dispatch(setFiltersProducts({ sameDayDeliv: !sameDayDeliv }));
              }}
            ></CustomedCheckBox>
          </OthersSection>
        </Section>
      </StyledSearchFilters>
    </ScreenContainerBasic>
  );
};

export default SearchFilters;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Section = styled.View`
  margin-bottom: 15px;
`;

const StyledSearchFilters = styled.View`
  min-height: 90%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  /* border: yellow 3px solid; */
`;

const Reset = styled.TouchableOpacity``;

const ResetText = styled.Text`
  color: #c29c1d;
  font-family: Lato-700;
  font-size: 18px;
`;

const TitleSection = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const InputsSection = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const StarsSection = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const NumberStars = styled.Text`
  font-family: Lato-400;
  font-size: 16px;
  color: #7d8fab;
`;

const OthersSection = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;
