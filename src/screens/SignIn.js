// react
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";
import { useForm } from "react-hook-form";
import { WBS_URL } from "../utils/globalVars";

// utils
import { grey } from "../assets/colors.json";
import { H1, Margin10 } from "../styles/global.styles";
import { REGEXP } from "../utils/globalVars";
import icons from "../assets/icons";

// components
import CustomedInputFormValidation from "../components/CustomedInputFormValidation";
import CustomedButton from "../components/CustomedButton";
import ScreenContainerModal from "../components/ScreenContainerModal";
import LogoSection from "../components/LogoSection";

// redux
import { useDispatch } from "react-redux";
import { setUser } from "../redux/actions/user.actions";

const { loginIcon, passwordIcon } = icons;

const Login = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    defaultValues: { email: "", password: "" },
  });

  const onSubmit = (data) => {
    const signInUser = async () => {
      try {
        const url = `${WBS_URL}/signin`;

        const body = { email: data.email, password: data.password };

        const response = await axios.post(url, body);

        if (response.data) {
          dispatch(
            setUser({
              userName: response.data.full_name,
              userToken: response.data.token,
            })
          );
        }
      } catch (error) {
        console.log("ERROR", error);
        if (error.errorMessage) alert("Wrong email or password");
      }
    };
    signInUser();
  };

  const outsideModal = <LogoSection />;

  return (
    <ScreenContainerModal backgroundColor="#027335" outsideModal={outsideModal}>
      <Margin10>
        <H1>Welcome back</H1>
      </Margin10>
      <Margin10>
        <Subtitle color={grey} fontSize={"14px"}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </Subtitle>
      </Margin10>
      <View style={{ marginTop: 15, marginBottom: 15 }}>
        <CustomedInputFormValidation
          placeholder={"louisanderson@gmail.com"}
          icon={loginIcon}
          height={"60px"}
          controllerName="email"
          control={control}
          error={errors.email}
          errorMessage={"Please enter a correct email adress"}
          patternValue={REGEXP.email}
        ></CustomedInputFormValidation>
        <View style={{ height: 10 }}></View>
        <CustomedInputFormValidation
          placeholder={"Password"}
          icon={passwordIcon}
          height={"60px"}
          secureTextEntry={true}
          controllerName="password"
          control={control}
          error={errors.password}
          errorMessage={"Please enter a password"}
        ></CustomedInputFormValidation>
      </View>

      <CustomedButton
        title={"SIGN IN"}
        theme={"dark"}
        width={"100%"}
        disabled={!isValid}
        onPressFunc={handleSubmit(onSubmit)}
      ></CustomedButton>
      <KeepSignInPasswordView>
        <TouchableOpacity>
          <Text
            style={{ color: "#027335", fontSize: 16, fontFamily: "Lato-700" }}
          >
            Forgot Pasword?
          </Text>
        </TouchableOpacity>
      </KeepSignInPasswordView>

      <Subtitle style={{ padding: 15, textAlign: "center" }}>
        Don't have an account?
      </Subtitle>
      <CustomedButton
        title={"CREATE AN ACCOUNT"}
        theme={"light"}
        onPressFunc={() => {
          navigation.navigate("SignUp");
        }}
      ></CustomedButton>
    </ScreenContainerModal>
  );
};

export default Login;

const KeepSignInPasswordView = styled.View`
  font-size: 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 15px 0;
`;

export const Subtitle = styled.Text`
  color: ${(props) => props.color || "#7D8FAB"};
  font-family: Lato-400;
  font-size: ${(props) => props.fontSize || "16px"};
`;
