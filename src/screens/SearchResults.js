// react
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useEffect, useState } from "react";
import axios from "axios";
import { WBS_URL } from "../utils/globalVars";

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  resetFiltersProducts,
  setProductsToDisplay,
} from "../redux/actions/products.actions";

// icons
import icons from "../assets/icons";
const { searchIcon, crossIcon, discountIcon } = icons;

// components
import CustomedInputBasic from "../components/CustomedInputBasic";
import ProductCard from "../components/ProductCard";
import ScreenContainer from "../components/ScreenContainer";

// styles
import { H2, Line } from "../styles/global.styles";
import { setFiltersProducts } from "../redux/actions/products.actions";

const SearchResults = ({}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(true);

  // all filters
  const filters = useSelector((state) => state.products.filters);

  // other filters
  const inputSearch = useSelector(
    (state) => state.products.filters.inputSearch
  );
  const productsToDisplay = useSelector(
    (state) => state.products.productsToDisplay
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const url = `${WBS_URL}/products/getProductsFiltered`;
        const body = {
          filters,
        };
        const response = await axios.post(url, body);
        dispatch(setProductsToDisplay({ productsToDisplay: response.data }));

        setIsLoading(false);
      } catch (error) {
        console.log({ SearchResultsError: error.message });
      }
    };
    fetchData();
  }, [filters]);

  const header = (
    <Header>
      <CrossButton
        onPress={() => {
          dispatch(resetFiltersProducts());
          navigation.navigate("Home");
        }}
      >
        {crossIcon}
      </CrossButton>
      <CustomedInputBasic
        value={inputSearch}
        filter={true}
        width={"86%"}
        height={"100%"}
        placeholder="Strawberry"
        icon={searchIcon}
        onPressFilterIconFunc={() => {
          navigation.navigate("SearchFilters");
        }}
        onChangeFunc={(event) => {
          dispatch(setFiltersProducts({ inputSearch: event }));
        }}
      ></CustomedInputBasic>
    </Header>
  );

  return (
    !isLoading && (
      <ScreenContainer header={header}>
        <MarginCorrection>
          <H2>Search Results</H2>
        </MarginCorrection>
        {productsToDisplay.map((product, index) => {
          return (
            <View key={index}>
              <ProductCard
                product={product}
                onPressFunc={() => {
                  navigation.navigate("ProductDetails", {
                    productId: product.id,
                  });
                }}
              ></ProductCard>
              <Line></Line>
            </View>
          );
        })}
      </ScreenContainer>
    )
  );
};
export default SearchResults;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const MarginCorrection = styled.View`
  margin-bottom: -10px;
`;

const CrossButton = styled.TouchableOpacity`
  margin-right: 10px;
`;
