// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, Image } from "react-native";
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigation } from "@react-navigation/core";
import { useSelector, useDispatch } from "react-redux";
import { WBS_URL } from "../utils/globalVars";

// components
import CustomedButton from "../components/CustomedButton";
import Counter from "../components/Counter";
import GoBack from "../components/GoBack";
import ScreenContainerModal from "../components/ScreenContainerModal";

// icons
import icons from "../assets/icons";
const { filledHeartWhiteIcon, fullStarIcon, deliveryIcon } = icons;

// redux
import {
  removeProductFromCart,
  addProductToCart,
} from "../redux/actions/shoppingCart.actions";

const ProductDetails = ({ route }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const { productId } = route.params;

  // local states
  const [product, setProduct] = useState();
  const [reviews, setReviews] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [quantity, setQuantity] = useState(0);

  // redux state
  const shoppingCart = useSelector((state) => state.shoppingCart.cart);

  useEffect(() => {
    const findQuantityProduct = (productId) => {
      let productNotFound = true;
      shoppingCart.forEach((productInShoppingCart) => {
        if (productInShoppingCart.id === productId) {
          setQuantity(productInShoppingCart.quantity);
          productNotFound = false;
        }
      });
      if (productNotFound) {
        setQuantity(0);
      }
    };

    findQuantityProduct(productId);
  }, [shoppingCart]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const url = `${WBS_URL}/products/getProductById`;
        const body = { productId };
        const response = await axios.post(url, body);
        setProduct(response.data);

        const url2 = `${WBS_URL}/reviews/getReviewsByProductId`;
        const body2 = { productId };
        const response2 = await axios.post(url2, body2);
        setReviews(response2.data);

        setIsLoading(false);
      } catch (error) {
        console.log({ SearchResultsError: error.message });
      }
    };
    fetchData();
  }, []);

  const header = (
    <GoBack
      title={"Go Back"}
      icon={filledHeartWhiteIcon}
      onPressArrowFunc={() => {
        navigation.goBack();
      }}
    ></GoBack>
  );

  const outsideModal = product && (
    <Image
      style={{ flex: 1 }}
      source={{
        uri: product.img_url
          ? `${WBS_URL}${product.img_url}`
          : "https://media.istockphoto.com/photos/fresh-fruit-display-in-open-food-market-in-rome-italy-picture-id492262592",
      }}
    ></Image>
  );

  return (
    !isLoading && (
      <ScreenContainerModal
        backgroundColor="white"
        header={header}
        outsideModal={outsideModal}
        statusBarStyle="dark"
      >
        <CategoryName>{product.category_name.toUpperCase()}</CategoryName>
        <ProductName>{product.name}</ProductName>
        <Description>{product.description}</Description>
        <RateSection>
          <Reviews>
            {fullStarIcon}
            <Rate>
              {reviews.average_rate} ({reviews.nb_reviews} reviews)
            </Rate>
          </Reviews>
          <DeliveryView>
            <Text>{deliveryIcon} </Text>
            <FreeDelivery> FREE DELIVERY</FreeDelivery>
          </DeliveryView>
        </RateSection>

        <PriceAndAmountSection>
          <PriceSection>
            <Title>Price</Title>
            {product.priceAfterDiscount ? (
              <PriceView>
                <PriceAfterDiscount>
                  $ {product.priceAfterDiscount}
                </PriceAfterDiscount>
                <Price textDecoration>$ {product.price}</Price>
              </PriceView>
            ) : (
              <Price>$ {product.price}</Price>
            )}
          </PriceSection>

          <CounterWrap>
            <Counter
              theme={"light"}
              quantity={quantity || 0}
              incFunc={() => {
                dispatch(addProductToCart(product));
              }}
              decFunc={() => {
                dispatch(removeProductFromCart(product));
              }}
            ></Counter>
          </CounterWrap>
        </PriceAndAmountSection>
        {/* <Discount></Discount> */}
        <ButtonWrap>
          <CustomedButton
            theme={"dark"}
            title={"ADD TO CART"}
            onPressFunc={() => {
              navigation.goBack();
            }}
          ></CustomedButton>
        </ButtonWrap>
      </ScreenContainerModal>
    )
  );
};
export default ProductDetails;

const CategoryName = styled.Text`
  font-family: Lato-900;
  color: #28b0ce;
  font-size: 16px;
  margin-bottom: 10px;
`;
const ProductName = styled.Text`
  font-family: Poppins-500;
  font-size: 20px;
  margin-bottom: 10px;
`;
const Description = styled.Text`
  font-family: Lato-400;
  font-size: 14px;
  margin-bottom: 20px;
`;

const RateSection = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

const Reviews = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Rate = styled.Text`
  margin-left: 10px;
  font-family: Lato-700;
  font-size: 14px;
`;

const DeliveryView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const FreeDelivery = styled.Text`
  color: #027335;
  font-family: Lato-700;
  font-size: 14px;
`;

const PriceAndAmountSection = styled.View`
  margin-bottom: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;
const PriceSection = styled.View``;

const PriceView = styled.View`
  display: flex;
  flex-direction: row;
`;

const Price = styled.Text`
  color: ${(props) => (props.textDecoration ? "#bfc9da" : "#303733")};
  font-family: Lato-400;
  font-size: 16px;
  margin: 10px;
  text-decoration: ${(props) => (props.textDecoration ? "line-through" : "")};
  text-decoration-color: #bfc9da;
`;

const PriceAfterDiscount = styled.Text`
  color: #303733;
  font-family: Lato-700;
  font-size: 24px;
`;

const Title = styled.Text`
  margin-bottom: 10px;
  color: #7d8fab;
  font-family: Lato-400;
  font-size: 14px;
`;

const CounterWrap = styled.View`
  width: 130px;
`;

const Discount = styled.View``;

const ButtonWrap = styled.View`
  padding-top: 20px;
`;
