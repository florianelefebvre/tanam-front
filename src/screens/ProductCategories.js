// react-native
import React from "react";
import styled from "styled-components/native";
import CategoryCard from "../components/CartegoryCard";
import { useNavigation } from "@react-navigation/native";

// components
import ScreenContainerBasic from "../components/ScreenContainerBasic";
import GoBack from "../components/GoBack";

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  resetFiltersProducts,
  setFiltersProducts,
} from "../redux/actions/products.actions";

const Categories = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const categories = useSelector((state) => state.products.categories);

  const header = (
    <GoBack
      title={"Categories"}
      onPressArrowFunc={() => {
        navigation.goBack();
      }}
    ></GoBack>
  );

  return (
    <ScreenContainerBasic statusBarStyle={"dark"} header={header}>
      <AlignView>
        <CategoriesSection>
          {categories.map((category) => {
            return (
              <CategoryCard
                key={category.id}
                category={category}
                size={"big"}
                onPressFunc={() => {
                  dispatch(setFiltersProducts({ categoryId: category.id }));
                  navigation.navigate("Products", {
                    categoryName: category.name,
                  });
                }}
              ></CategoryCard>
            );
          })}
        </CategoriesSection>
      </AlignView>
    </ScreenContainerBasic>
  );
};
export default Categories;

const AlignView = styled.View`
  display: flex;
  justify-content: center;
  min-height: 100%;
`;

const CategoriesSection = styled.View`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
`;
