// react
import React, { useEffect } from "react";
import styled from "styled-components/native";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { useForm, Controller } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { WBS_URL } from "../utils/globalVars";

// components
import ScreenContainer from "../components/ScreenContainer";
import GoBack from "../components/GoBack";
import CustomedInputFormValidation from "../components/CustomedInputFormValidation";
import CustomedButton from "../components/CustomedButton";
import PaymentMethodButton from "../components/PaymentMethodButton";

// styled components
import { H3 } from "../styles/global.styles";

// utils & assets
import creditCard from "../assets/images/creditCard.png";
import icons from "../assets/icons";
const { paypalGreyIcon, creditCardGreyIcon, cashOnDelivGreyIcon } = icons;
import { REGEXP } from "../utils/globalVars";
import stepperPaymentForm from "../assets/images/stepperPaymentForm.png";

// redux
import { setPaymentMethod } from "../redux/actions/user.actions";
import { resetShoppingCart } from "../redux/actions/shoppingCart.actions";

const PaymentForm = ({}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // local states

  const [paymentStatus, setPaymentStatus] = useState("");

  // redux states
  const paymentDetails = useSelector((state) => state.user.paymentDetails);
  const shippingDetails = useSelector((state) => state.user.shippingDetails);
  const userToken = useSelector((state) => state.user.userToken);
  const cart = useSelector((state) => state.shoppingCart.cart);

  const paymentMethod = useSelector(
    (state) => state.user.paymentDetails.paymentMethod
  );

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    defaultValues: paymentDetails,
  });

  const onSubmit = (formData) => {
    alert("The payment has been validated by your bank");

    setPaymentStatus("OK");
  };

  const createOrder = async () => {
    try {
      const url = `${WBS_URL}/orders/createOrder`;
      const body = {
        userToken,
        shippingDetails,
        paymentMethod,
        cart,
      };

      const response = await axios.post(url, body);
      if (response.data) {
        navigation.navigate("MyOrders");
        dispatch(resetShoppingCart());
        dispatch(
          setPaymentMethod({
            cashOnDelivery: false,
            creditCard: true,
            paypalMethod: false,
          })
        );
      }
    } catch (error) {
      if (error.response) alert(error.response.data.errorMessage);
      console.log(error);
    }
  };

  useEffect(() => {
    if (paymentMethod.paypalMethod || paymentMethod.cashOnDelivery) {
      createOrder();
    } else if (paymentMethod.creditCard && paymentStatus === "OK") {
      createOrder();
    }
  }, [paymentMethod, paymentStatus]);

  const header = (
    <GoBack
      title={"Checkout"}
      onPressArrowFunc={() => {
        navigation.navigate("ShoppingCart");
      }}
    ></GoBack>
  );

  return (
    <ScreenContainer header={header}>
      <StepperWrap>
        <StepperImage source={stepperPaymentForm} resizeMode="contain" />
      </StepperWrap>
      <PaymentMethod>
        <PaymentMethodButton
          label={"Cash On Delivery"}
          icon={cashOnDelivGreyIcon}
          selectedPaymentMethod={paymentMethod.cashOnDelivery}
          onPressFunc={() => {
            dispatch(
              setPaymentMethod({
                cashOnDelivery: !paymentMethod.cashOnDelivery,
                creditCard: false,
                paypalMethod: false,
              })
            );
          }}
        ></PaymentMethodButton>

        <PaymentMethodButton
          label={"Credit Card"}
          icon={creditCardGreyIcon}
          selectedPaymentMethod={paymentMethod.creditCard}
          onPressFunc={() => {
            dispatch(
              setPaymentMethod({
                cashOnDelivery: false,
                creditCard: !paymentMethod.creditCard,
                paypalMethod: false,
              })
            );
          }}
        ></PaymentMethodButton>
        <PaymentMethodButton
          label={"Paypal Method"}
          icon={paypalGreyIcon}
          selectedPaymentMethod={paymentMethod.paypalMethod}
          onPressFunc={() => {
            dispatch(
              setPaymentMethod({
                cashOnDelivery: false,
                creditCard: false,
                paypalMethod: !paymentMethod.paypalMethod,
              })
            );
          }}
        ></PaymentMethodButton>
      </PaymentMethod>
      <CardWrap>
        <CreditCard source={creditCard} resizeMode="contain" />
      </CardWrap>

      <H3>Card Holder Name</H3>
      <CustomedInputFormValidation
        placeholder="Louis Anderson"
        height="54px"
        controllerName="cardHolderName"
        control={control}
        error={errors.cardHolderName}
        errorMessage={"This field is required"}
        patternValue={REGEXP.alpha}
      ></CustomedInputFormValidation>
      <H3>Card Number</H3>
      <CustomedInputFormValidation
        placeholder="6245621462146214"
        height="54px"
        controllerName="cardNumber"
        control={control}
        error={errors.cardNumber}
        errorMessage={"Pleaser enter a correct credit card number "}
        patternValue={REGEXP.creditCardNumber}
      ></CustomedInputFormValidation>
      <DisplayRowView>
        <SplitView>
          <H3>MM/YY</H3>
          <CustomedInputFormValidation
            placeholder="Enter here"
            height="54px"
            controllerName="cardMonthYear"
            control={control}
            error={errors.cardMonthYear}
            errorMessage={"Pleaser enter data to correct format :  MM/YY"}
            patternValue={REGEXP.creditCardMonthYear}
          ></CustomedInputFormValidation>
        </SplitView>
        <SplitView>
          <H3>CVV</H3>
          <CustomedInputFormValidation
            placeholder="Enter here"
            height="54px"
            controllerName="cardCvv"
            control={control}
            error={errors.cardCvv}
            errorMessage={"CVV not correct"}
            patternValue={REGEXP.creditCardCvv}
            secureTextEntry={true}
          ></CustomedInputFormValidation>
        </SplitView>
      </DisplayRowView>

      <ButtonMargin>
        <CustomedButton
          title="MAKE PAYMENT"
          theme="dark"
          disabled={!isValid}
          onPressFunc={handleSubmit(onSubmit)}
        ></CustomedButton>
      </ButtonMargin>
    </ScreenContainer>
  );
};
export default PaymentForm;

const StepperImage = styled.Image`
  width: 100%;
`;

const StepperWrap = styled.View`
  height: 100px;
  overflow: hidden;
  display: flex;
  justify-content: center;
`;

const CardImageWrap = styled.View`
  height: 100px;
  overflow: hidden;
  display: flex;
  justify-content: center;
`;

const PaymentMethod = styled.View`
  display: flex;
  flex-direction: row;
`;

const DisplayRowView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const SplitView = styled.View`
  width: 47%;
`;

const CreditCard = styled.Image`
  width: 100%;
`;

const CardWrap = styled.View`
  margin-top: 20px;
  height: 260px;
  overflow: hidden;
  display: flex;
  justify-content: center;
`;

const ButtonMargin = styled.View`
  margin-top: 30px;
  margin-bottom: 40px;
`;
