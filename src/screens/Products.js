// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, ActivityIndicator } from "react-native";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { useState, useEffect } from "react";
import { WBS_URL } from "../utils/globalVars";

// icons
import icons from "../assets/icons";
const { searchIcon, filterIconBlack } = icons;

// components
import ProductCard from "../components/ProductCard";
import ScreenContainer from "../components/ScreenContainer";
import GoBack from "../components/GoBack";

// redux
import { useSelector, useDispatch } from "react-redux";
import {
  resetFiltersProducts,
  setFiltersProducts,
  setProductsToDisplay,
} from "../redux/actions/products.actions";

const Products = ({ route }) => {
  const navigation = useNavigation();
  const { categoryName } = route.params;
  const dispatch = useDispatch();

  // local state
  const [isLoading, setIsLoading] = useState(true);

  // redux state

  const filters = useSelector((state) => state.products.filters);
  const productsToDisplay = useSelector(
    (state) => state.products.productsToDisplay
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const url = `${WBS_URL}/products/getProductsFiltered`;
        const body = {
          filters,
        };
        const response = await axios.post(url, body);
        dispatch(setProductsToDisplay({ productsToDisplay: response.data }));
        setIsLoading(false);
      } catch (error) {
        console.log({ ProductsError: error.message });
      }
    };
    fetchData();
  }, [filters]);

  const header = (
    <GoBack
      title={categoryName}
      icon={filterIconBlack}
      onPressIconFunc={() => {
        navigation.navigate("SearchFilters");
      }}
      onPressArrowFunc={() => {
        dispatch(resetFiltersProducts());
        navigation.goBack();
      }}
    ></GoBack>
  );

  return (
    !isLoading && (
      <ScreenContainer header={header}>
        <Searchbar
          onPress={() => {
            navigation.navigate("SearchResults");
          }}
        >
          {searchIcon}
          <SearchInput>Search beverages of foods</SearchInput>
        </Searchbar>

        {productsToDisplay.map((product) => {
          return (
            <ProductCard
              key={product.id}
              product={product}
              onPressFunc={() => {
                navigation.navigate("ProductDetails", {
                  productId: product.id,
                });
              }}
            ></ProductCard>
          );
        })}
      </ScreenContainer>
    )
  );
};
export default Products;

const Searchbar = styled.TouchableOpacity`
  margin-bottom: 20px;
  height: 54px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 20px;
  border: solid #e8eff3 1px;
  border-radius: 12px;
`;

const SearchInput = styled.Text`
  color: #7d8fab;
  font-family: Lato-400;
  font-size: 16px;
  margin-left: 20px;
`;
