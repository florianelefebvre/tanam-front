// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, Dimensions } from "react-native";
import { useNavigation } from "@react-navigation/core";

// components
import GoBack from "../components/GoBack";
import ScreenContainerModal from "../components/ScreenContainerModal";

// assets & styles
import icons from "../assets/icons";
const { chatIcon, checkmarkCircleIcon, phoneIcon } = icons;
import mapItinerary from "../assets/images/mapItinerary.png";

const TrackingOrder = ({}) => {
  const navigation = useNavigation();

  const header = (
    <GoBack
      title={"Track Order"}
      onPressArrowFunc={() => {
        navigation.goBack();
      }}
    ></GoBack>
  );

  const outsideModal = (
    <MapWrap>
      <MapImage source={mapItinerary} resizeMode={"contain"}></MapImage>
    </MapWrap>
  );

  return (
    <ScreenContainerModal
      backgroundColor={"white"}
      header={header}
      outsideModal={outsideModal}
      statusBarStyle={"dark"}
    >
      <StyledDeliveryPersonView>
        <PictureNameIdView>
          <PictureDeliv></PictureDeliv>
          <FlexColomn>
            <DelivPerson>James King</DelivPerson>
            <IdView>
              <Text>{checkmarkCircleIcon}</Text>
              <IdDeliv> ID #0012345</IdDeliv>
            </IdView>
          </FlexColomn>
        </PictureNameIdView>
        <PhoneChatIconsView>
          <IconButton>
            <Text>{phoneIcon}</Text>
          </IconButton>
          <IconButton>
            <Text> {chatIcon}</Text>
          </IconButton>
        </PhoneChatIconsView>
      </StyledDeliveryPersonView>
    </ScreenContainerModal>
  );
};
export default TrackingOrder;

const MapWrap = styled.View`
  flex: 1;
  justify-content: center;
  overflow: hidden;
  margin-top: 20px;
`;

const MapImage = styled.Image`
  max-width: 100%;
`;

const PictureDeliv = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #c4c4c4;
  border-radius: 12px;
  height: 53px;
  width: 56px;
`;

const FlexColomn = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-left: 20px;
`;

const PictureNameIdView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 3;
`;

const PhoneChatIconsView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  flex: 1;
`;

const DelivPerson = styled.Text`
  color: #303733;
  font-family: Lato-700;
  font-size: 18px;
`;

const IdDeliv = styled.Text`
  color: #7d8fab;
  font-family: Lato-400;
  font-size: 14px;
  margin-left: 5px;
`;

const IconButton = styled.TouchableOpacity``;

const IdView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 7px;
`;

const StyledDeliveryPersonView = styled.View`
  display: flex;
  flex-direction: row;
`;
