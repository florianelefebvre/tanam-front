// react
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useState, useEffect } from "react";

// redux
import { useSelector, useDispatch } from "react-redux";
import { setCartPrice } from "../redux/actions/shoppingCart.actions";

// components
import GoBack from "../components/GoBack";
import ScreenContainerBasic from "../components/ScreenContainerBasic";
import CustomedButton from "../components/CustomedButton";
import ShoppingCartProductCard from "../components/ShoppingCartProductCard";

const ShoppingCart = ({}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const shoppingCart = useSelector((state) => state.shoppingCart.cart);
  const subtotal = useSelector((state) => state.shoppingCart.subtotal);
  const taxe = useSelector((state) => state.shoppingCart.taxe);
  const total = useSelector((state) => state.shoppingCart.total);

  useEffect(() => {
    dispatch(setCartPrice());
  }, [shoppingCart]);

  const header = (
    <GoBack
      title={"Shopping Cart"}
      onPressArrowFunc={() => {
        navigation.goBack();
      }}
    ></GoBack>
  );

  const footer = (
    <TotalView>
      <SpaceBetweenView>
        <GreyText>Subtotal</GreyText>
        <GreyText>$ {subtotal.toFixed(1)}</GreyText>
      </SpaceBetweenView>

      <SpaceBetweenView>
        <GreyText>TAX (2%)</GreyText>
        <GreyText>-$ {taxe.toFixed(1)}</GreyText>
      </SpaceBetweenView>

      <SpaceBetweenView>
        <Lato70018>Total</Lato70018>
        <Lato70018>$ {total.toFixed(1)}</Lato70018>
      </SpaceBetweenView>
      <CustomedButton
        title="CHECKOUT"
        theme="dark"
        onPressFunc={() => {
          if (!shoppingCart[0]) {
            return alert("You have nothing in your shopping cart");
          }
          navigation.navigate("ShippingAdress");
        }}
      ></CustomedButton>
    </TotalView>
  );

  return (
    <ScreenContainerBasic header={header} footer={footer}>
      <StyledShoppingCart>
        {shoppingCart.map((product, index) => {
          return (
            <ShoppingCartProductCard
              key={index}
              product={product}
            ></ShoppingCartProductCard>
          );
        })}
      </StyledShoppingCart>
    </ScreenContainerBasic>
  );
};
export default ShoppingCart;

const StyledShoppingCart = styled.View`
  margin-top: 10px;
  margin-bottom: 30px;
  /* border: solid red 2px; */
  flex: 1;
  min-height: 90%;
  /* align-items: flex-end; */
`;

const TotalView = styled.View`
  padding: 20px 0;
  border-top-color: #bfc9da;
  border-top-width: 1px;
  border-style: solid;
  margin-top: 20px;
`;

const SpaceBetweenView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 5px 0;
`;

const GreyText = styled.Text`
  font-family: Lato-700;
  font-size: 14px;
  color: #7d8fab;
`;

const Lato70018 = styled.Text`
  font-family: Lato-700;
  font-size: 18px;
  color: #293041;
  margin-bottom: 20px;
`;
