// react
import React from "react";
import styled from "styled-components/native";
import { View, Text, TextInput, Button } from "react-native";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";

// components
import ScreenContainer from "../components/ScreenContainer";
import GoBack from "../components/GoBack";
import CustomedInputFormValidation from "../components/CustomedInputFormValidation";
import CustomedButton from "../components/CustomedButton";

// styled components
import { H3 } from "../styles/global.styles";
import { useNavigation } from "@react-navigation/native";

// utils & assets
import { REGEXP } from "../utils/globalVars";
import stepperShippingAddress from "../assets/images/stepperShippingAddress.png";

// redux
import { setShippingDetails } from "../redux/actions/user.actions";

const ShippingAdress = ({}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const shippingDetails = useSelector((state) => state.user.shippingDetails);

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    defaultValues: shippingDetails,
  });

  const onSubmit = (formData) => {
    dispatch(setShippingDetails({ formData }));
    navigation.navigate("PaymentForm");
  };

  const header = (
    <GoBack
      title={"Checkout"}
      onPressArrowFunc={() => {
        navigation.goBack();
      }}
    ></GoBack>
  );

  return (
    <ScreenContainer header={header}>
      <StepperWrap>
        <StepperImage
          source={stepperShippingAddress}
          resizeMode="contain"
        ></StepperImage>
      </StepperWrap>

      <H3>Full Name</H3>
      <CustomedInputFormValidation
        placeholder="Louis Anderson"
        height="54px"
        controllerName="fullName"
        control={control}
        error={errors.fullName}
        errorMessage={"This Field is required"}
        patternValue={REGEXP.alpha}
      ></CustomedInputFormValidation>

      <H3>Email Adress</H3>
      <CustomedInputFormValidation
        placeholder="louisanderson@gmail.com"
        controllerName="email"
        control={control}
        error={errors.email}
        height="54px"
        errorMessage={"Please enter a correct email adress"}
        patternValue={REGEXP.email}
      ></CustomedInputFormValidation>

      <H3>Phone</H3>
      <CustomedInputFormValidation
        placeholder="Enter your phone number"
        height="54px"
        controllerName="phone"
        control={control}
        error={errors.phone}
        errorMessage={"Please enter a correct phone number"}
        patternValue={REGEXP.phone}
      ></CustomedInputFormValidation>

      <H3>Address</H3>
      <CustomedInputFormValidation
        placeholder="Type your home adress"
        height="54px"
        controllerName="address"
        control={control}
        error={errors.address}
        errorMessage={"This Field is required"}
        patternValue={REGEXP.alphaNumeric}
      ></CustomedInputFormValidation>
      <DisplayRowView>
        <SplitView>
          <H3>ZIP code</H3>
          <CustomedInputFormValidation
            placeholder="Enter here"
            height="54px"
            controllerName="zipCode"
            control={control}
            error={errors.zipCode}
            errorMessage={"Please enter a correct zip code"}
            patternValue={REGEXP.numeric}
          ></CustomedInputFormValidation>
        </SplitView>
        <SplitView>
          <H3>City</H3>
          <CustomedInputFormValidation
            placeholder="Enter here"
            height="54px"
            controllerName="city"
            control={control}
            error={errors.city}
            errorMessage={"Please enter a correct city"}
            patternValue={REGEXP.alpha}
          ></CustomedInputFormValidation>
        </SplitView>
      </DisplayRowView>

      <H3>Country</H3>
      <CustomedInputFormValidation
        placeholder="Country"
        height="54px"
        controllerName="country"
        control={control}
        error={errors.country}
        errorMessage={"Please enter a correct country"}
        patternValue={REGEXP.alpha}
      ></CustomedInputFormValidation>
      <ButtonMargin>
        <CustomedButton
          title="NEXT"
          theme="dark"
          disabled={!isValid}
          onPressFunc={handleSubmit(onSubmit)}
        ></CustomedButton>
      </ButtonMargin>
    </ScreenContainer>
  );
};
export default ShippingAdress;

const StepperImage = styled.Image`
  width: 100%;
`;

const StepperWrap = styled.View`
  height: 100px;
  overflow: hidden;
  display: flex;
  justify-content: center;
`;

const DisplayRowView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const SplitView = styled.View`
  width: 47%;
`;

const ButtonMargin = styled.View`
  margin-top: 30px;
  margin-bottom: 40px;
`;
