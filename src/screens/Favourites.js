// react-native
import React from "react";
import styled from "styled-components/native";
import { View, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const Favorites = ({}) => {
  return (
    <SafeAreaView
      style={{ flex: 1, backgroundColor: "white" }}
      edges={["right", "top", "left"]}
    >
      <StyledFavorites>
        <Text>Favorites</Text>
      </StyledFavorites>
    </SafeAreaView>
  );
};
export default Favorites;

const StyledFavorites = styled.View``;
