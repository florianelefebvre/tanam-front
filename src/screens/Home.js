// react
import React, { useEffect, useState } from "react";
import styled from "styled-components/native";
import { View, Text, TouchableOpacity } from "react-native";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
// import { WBS_URL } from "@env";
import { WBS_URL } from "../utils/globalVars";

// redux
import { useSelector, useDispatch } from "react-redux";
import { setLogout } from "../redux/actions/user.actions";
import {
  resetFiltersProducts,
  setCategories,
  setFiltersProducts,
} from "../redux/actions/products.actions";

// components
import CategoryCard from "../components/CartegoryCard";
import PopularDealCard from "../components/PopularDealCard";
import ScreenContainer from "../components/ScreenContainer";

// assets & styled & icons
import discounts from "../assets/data/discounts.json";
import { H1, H2 } from "../styles/global.styles";
import icons from "../assets/icons";
const { searchIcon, arrowRightIcon, logOutIcon } = icons;

const Home = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // redux states
  const userName = useSelector((state) => state.user.userName);
  const categories = useSelector((state) => state.products.categories);
  const filters = useSelector((state) => state.products.filters);

  // local states
  const [isLoading, setIsLoading] = useState(true);
  const [categoriesFetching, setCategoriesFetching] = useState(true);
  const [popularProducts, setPopularProducts] = useState();
  const [greeting, setGreeting] = useState("");

  useEffect(() => {
    const fetchCategories = async () => {
      const urlCategories = `${WBS_URL}/categories/getCategories`;
      const categories = await axios.get(urlCategories);
      dispatch(setCategories(categories.data));
      setCategoriesFetching(false);
    };
    fetchCategories();
    const today = new Date();
    const time = today.getHours();

    if (time < 12) setGreeting("Good Morning");
    else if (time < 17) setGreeting("Good Afternoon");
    else setGreeting("Good evening");
  }, []);

  useEffect(() => {
    if (!categoriesFetching) {
      const fetchProductsToDisplay = async () => {
        try {
          const url = `${WBS_URL}/products/getPopularProducts`;
          const response = await axios.get(url);
          setPopularProducts(response.data);

          setIsLoading(false);
        } catch (error) {
          console.log({ HomeError: error.message, error: error });
        }
      };
      fetchProductsToDisplay();
    }
  }, [filters, categoriesFetching]);

  const header = (
    <Header>
      <GreetingSection>
        <Greeting>{greeting}</Greeting>
        <H1>{userName ? userName : "YOU"} 👋</H1>
      </GreetingSection>

      <TouchableOpacity
        onPress={() => {
          dispatch(setLogout());
        }}
      >
        <Text>{logOutIcon}</Text>
      </TouchableOpacity>
    </Header>
  );

  return (
    !isLoading && (
      <ScreenContainer statusBarStyle={"dark"} tabBar={true} header={header}>
        <Searchbar
          onPress={() => {
            dispatch(resetFiltersProducts());
            navigation.navigate("SearchResults");
          }}
        >
          {searchIcon}
          <SearchInput>Search beverages or food</SearchInput>
        </Searchbar>

        <H2View>
          <H2>Categories</H2>
          <ArrowButton
            onPress={() => {
              navigation.navigate("ProductCategories");
            }}
          >
            <Text>{arrowRightIcon}</Text>
          </ArrowButton>
        </H2View>

        <Categories horizontal={true}>
          {categories.map((category) => {
            return (
              <CategoryCard
                key={category.id}
                size={"small"}
                category={category}
                onPressFunc={() => {
                  dispatch(setFiltersProducts({ categoryId: category.id }));
                  navigation.navigate("Products", {
                    categoryName: category.name,
                  });
                }}
              ></CategoryCard>
            );
          })}
        </Categories>

        <H2>Popular Deals</H2>

        <PopularDealsSection>
          {popularProducts.map((product) => {
            return (
              <PopularDealCard
                key={product.id}
                product={product}
                onPressFunc={() => {
                  navigation.navigate("ProductDetails", {
                    productId: product.id,
                  });
                }}
              ></PopularDealCard>
            );
          })}
        </PopularDealsSection>
      </ScreenContainer>
    )
  );
};

export default Home;

const H2View = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const ArrowButton = styled.TouchableOpacity``;

const Header = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  /* border: solid 2px red; */
  align-items: center;
`;

const GreetingSection = styled.View``;

const Greeting = styled.Text`
  font-family: Lato-400;
  font-size: 14px;
`;

const Searchbar = styled.TouchableOpacity`
  margin-top: 20px;
  height: 54px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 20px;
  border: solid #e8eff3 1px;
  border-radius: 12px;
`;

const SearchInput = styled.Text`
  color: #7d8fab;
  font-family: Lato-400;
  font-size: 16px;
  margin-left: 20px;
`;

const Discounts = styled.ScrollView`
  display: flex;
  flex-direction: row;
`;

const DiscountCard = styled.View`
  background-color: #86c3d7;
  border-radius: 12px;
  padding: 30px;
  margin-right: 10px;
  height: 168px;
  width: 280px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const DiscountTitle = styled.Text`
  font-family: Lato-400;
  color: #203647;
  font-size: 12px;
`;

const DiscountType = styled.Text`
  font-family: Lato-900;
  color: #203647;
  padding: 8px 0;
  font-size: 28px;
`;

const DiscountPurchaseConditions = styled.Text`
  font-family: Lato-400;
  color: #203647;
  font-size: 10px;
`;

const Categories = styled.ScrollView`
  margin: 0 -30px;
  padding: 0 30px;
`;

const PopularDealsSection = styled.View`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
`;
