import styled from "styled-components/native";

export const H1 = styled.Text`
  font-family: Poppins-700;
  font-size: 24px;
`;

export const H2 = styled.Text`
  font-family: Lato-700;
  font-size: 18px;
  margin: 20px 0;
  color: #303733;
`;

export const H3 = styled.Text`
  font-family: Poppins-500;
  font-size: 14px;
  padding-bottom: 15px;
  padding-top: 20px;
  color: black;
  /* border: solid red 2px; */
`;

export const Line = styled.View`
  border-bottom-color: #e8eff3;
  border-bottom-width: 1px;
`;
export const Lato40012 = styled.Text`
  font-family: Lato-400;
  font-size: 12px;
  color: #7d8fab;
  margin-top: 5px; ;
`;

export const Lato70016 = styled.Text`
  font-family: Lato-700;
  font-size: 16px;
  color: #303733;
`;

export const Lato70014 = styled.Text`
  font-family: Lato-700;
  font-size: 14px;
  color: #7d8fab;
  margin-top: 5px; ;
`;

export const Lato70018 = styled.Text`
  font-family: Lato-400;
  font-size: 12px;
  color: #7d8fab;
  margin-top: 5px; ;
`;

export const Margin10 = styled.View`
  margin-bottom: 10px;
`;
