// react
import React from "react";

// sign screens
import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";

// other screens
import SearchFilters from "../screens/SearchFilters";
import SearchResults from "../screens/SearchResults";
import ProductCategories from "../screens/ProductCategories";
import Products from "../screens/Products";
import ProductDetails from "../screens/ProductDetails";

// checkout screens
import ShippingAdress from "../screens/ShippingAdress";
import PaymentForm from "../screens/PaymentForm";

// navigation
import HomeTabScreen from "./HomeTabScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
const Stack = createStackNavigator();

// redux
import { useSelector, useDispatch } from "react-redux";

const NavContainer = () => {
  const userToken = useSelector((state) => state.user.userToken);
  // const userToken = "123";

  return (
    <NavigationContainer>
      {userToken ? (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="HomeTabScreen" component={HomeTabScreen} />
          <Stack.Screen name="SearchFilters" component={SearchFilters} />
          <Stack.Screen name="SearchResults" component={SearchResults} />
          <Stack.Screen
            name="ProductCategories"
            component={ProductCategories}
          />
          <Stack.Screen name="Products" component={Products} />
          <Stack.Screen name="ProductDetails" component={ProductDetails} />

          <Stack.Screen name="ShippingAdress" component={ShippingAdress} />
          <Stack.Screen name="PaymentForm" component={PaymentForm} />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

export default NavContainer;
