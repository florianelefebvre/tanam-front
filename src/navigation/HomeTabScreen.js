// react
import React, { useEffect, useState } from "react";

// icons
import {
  MaterialIcons,
  FontAwesome,
  Feather,
  Entypo,
} from "@expo/vector-icons";

// navigation
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
const Tab = createBottomTabNavigator();

// tabBar screens
import Home from "../screens/Home";
import MyOrders from "../screens/MyOrders";
import TrackingOrders from "../screens/TrackingOrders";
import ShoppingCart from "../screens/ShoppingCart";
import Favorites from "../screens/Favourites";

const HomeTabScreen = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarIcon: ({ color, size }) => {
          // déplacer les icons dans fichier icons
          switch (route.name) {
            case "Home":
              return <Entypo name={"home"} size={size} color={color} />;
            case "MyOrders":
              return <Feather name="package" size={24} color={color} />;
            case "ShoppingCart":
              return <Entypo name="shopping-cart" size={24} color={color} />;
            // case "Favorites":
            //   return <MaterialIcons name="favorite" size={24} color={color} />;
            case "TrackingOrders":
              return <FontAwesome name="circle" size={30} color={"#C4C4C4"} />;
          }
        },
        tabBarActiveTintColor: "#027335",
        tabBarInactiveTintColor: "#7D8FAB",
        tabBarShowLabel: false,
      })}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="MyOrders" component={MyOrders} />
      <Tab.Screen
        options={{
          tabBarStyle: { display: "none" },
          tabBarLabel: "",
        }}
        name="ShoppingCart"
        component={ShoppingCart}
      />
      {/* <Tab.Screen
        options={{
          tabBarStyle: { display: "none" },
        }}
        name="Favorites"
        component={Favorites}
      /> */}
      <Tab.Screen
        options={{
          tabBarStyle: { display: "none" },
        }}
        name="TrackingOrders"
        component={TrackingOrders}
      />
    </Tab.Navigator>
  );
};

export default HomeTabScreen;
